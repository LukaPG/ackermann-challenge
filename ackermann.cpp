#include <string>
#include <iostream>
#include <chrono>
#include <ctime>
#include <unordered_map>
#include <tuple>
#include "boost/multiprecision/cpp_int.hpp"
#include "boost/functional/hash.hpp"

using bint = boost::multiprecision::cpp_int;

// Unordered map user for memoization in the int_pow function
std::unordered_map<bint, bint> powers;


//big integer exponentiation
//TODO optimize!!!
bint int_pow(bint exponent){
    if(powers.find(exponent) != powers.end()){
        return powers[exponent];
    }else{
        bint base(2);
        bint result(1);
        while (exponent){
            if (exponent & 1){
                result *= base;
            }
            exponent >>= 1;
            base *= base;
        }
        powers[exponent] = result;
        return result;
    }
}
// Unordered map of
// pair<uint, bigint> => bigint
// used for memoization purposes.
std::unordered_map<std::pair<unsigned, bint>, bint, boost::hash<std::pair<unsigned, bint>>> done;

int levels_of_recursion = 0;
bint ackermann(unsigned m, bint n){
    levels_of_recursion++;

    auto params = std::make_pair(m, n);
    if(done.find(params) != done.end()){
        return done[params];
    }

    bint result;
    switch (m){
        case 0:
            result = n+1;
            done[params] = result;
            return result;
        case 1:
            result = n+2;
            done[params] = result;
            return result;
        case 2:
            result = 3 + 2*n;
            done[params] = result;
            return result;
        case 3:
            result = 5 + 8 * (int_pow(n) - 1);
            done[params] = result;
            return result;
        default:
            if (n==0){
                result = ackermann(m-1, 1);
                done[params] = result;
                return result;
            }else{
                result = ackermann(m-1, ackermann(m, n-1));
                done[params] = result;
                return result;
            }
    }
}

int main(int argc, char* argv[]){
    if(argc < 3){
        for (int m=0; m<5; m++){
            for (int n=0; n<20; n++){
                levels_of_recursion = 0;
                std::clock_t c_start = std::clock();
                auto t_start = std::chrono::high_resolution_clock::now();

                std::cout << "Ackermann(" << m << ", " << n << ") = " << ackermann(m, n) << std::endl;

                std::clock_t c_end = std::clock();
                auto t_end = std::chrono::high_resolution_clock::now();
                std::cout << std::fixed << std::setprecision(2) << "CPU time used: "
                          << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms\n";
                std::cout << levels_of_recursion << " levels of recursion.\n\n";
            }
        }
        return 0;
    }else if (argc == 3){
        unsigned m = atoi(argv[1]);
        unsigned n = atoi(argv[2]);

        std::cout << "m = " << m << ", n = " << n << std::endl;
        std::cout << "Ackermann(" << m << ", " << n << ") = " << ackermann(m, n) << std::endl;
        return 0;
    }
    return 1;
}
